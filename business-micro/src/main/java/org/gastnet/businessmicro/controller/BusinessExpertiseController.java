package org.gastnet.businessmicro.controller;

import org.gastnet.businessmicro.entity.BusinessExpertise;
import org.gastnet.businessmicro.service.BusinessExpertiseService;
import org.gastnet.businessmicro.service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/business-expertises")
public class BusinessExpertiseController {
	
	@Autowired
	private BusinessExpertiseService service;
	@Autowired
	private BusinessService bService;

	@PostMapping("/{businessId}")
	public  void add(@RequestBody BusinessExpertise expertise,@PathVariable long businessId) {
		expertise.setBusiness(bService.findById(businessId));
		service.save(expertise);
	}
}
